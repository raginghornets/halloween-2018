﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SidescrollerPlayerMovement : MonoBehaviour {

	public float movementSpeed = 500.0f;
	public float jumpHeight = 250.0f;
	
	private Rigidbody2D rb2d;

	void Start() {
		rb2d = GetComponent<Rigidbody2D>();
	}
	
	void FixedUpdate () {
		if (rb2d.velocity.y < 0.1f && rb2d.velocity.y > -0.1f) {
			if (Input.GetKeyDown("space")) {
      	Jump(jumpHeight);
			}
			Walk(Input.GetAxisRaw("Horizontal"), movementSpeed);
		} else if (rb2d.velocity.y > 0.1f || rb2d.velocity.y < -0.1f) {
			Walk(Input.GetAxisRaw("Horizontal"), movementSpeed / 2);
		}
	}

	void Jump(float jumpHeight) {
		rb2d.AddForce(transform.up * jumpHeight);
	}

	void Walk(float direction, float speed) {
		rb2d.velocity = new Vector2(
			direction * speed * Time.deltaTime, 
			rb2d.velocity.y
		);
	}

}
