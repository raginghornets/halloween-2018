﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownPlayerMovement : MonoBehaviour {

	public float movementSpeed = 10.0f;
	
	void FixedUpdate () {
		transform.position = new Vector2(
			transform.position.x + 
			Input.GetAxisRaw("Horizontal") * 
			movementSpeed * Time.deltaTime, 
			transform.position.y + 
			Input.GetAxisRaw("Vertical") * 
			movementSpeed * Time.deltaTime
		);
	}
	
}
