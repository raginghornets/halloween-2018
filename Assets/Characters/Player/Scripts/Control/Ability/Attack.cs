﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {

	public GameObject projectile;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.F)) {
			ActionShootProjectile(projectile);
		}
	}
	public void ActionShootProjectile(GameObject projectile) {
			GameObject projectileClone = Instantiate(projectile, Camera.main.ScreenToWorldPoint(Input.mousePosition), transform.rotation);
			projectileClone.transform.localScale /= 2;
			projectileClone.AddComponent<Rigidbody2D>();
			projectileClone.GetComponent<Rigidbody2D>().gravityScale = 0;
			Destroy(projectileClone, 0.5f);
	}
}

